#include "Barom.h"
I2C I2C;

/* CONSTRUCTOR */
/***************/
Barom::Barom(uint8_t SCLpin, uint8_t SDApin, float elevation, Unit::Elevation elevUnit, OSSmode OSS)
{
   I2C.init(SCLpin, SDApin, Register.barometer);
   elevInMeters = elevation;
   convertElevation(&elevInMeters, elevUnit, Unit::meters);
   this->OSS = OSS;
   
   // Get calibration parameters (Registers 0xAA - 0xBF, automatically incremented)
   I2C.write(0xAA);
   AC1 = I2C.read(2);
   AC2 = I2C.read(2);
   AC3 = I2C.read(2);
   AC4 = I2C.read(2);
   AC5 = I2C.read(2);
   AC6 = I2C.read(2);
   
   BB1 = I2C.read(2);
   BB2 = I2C.read(2);
   
   MB = I2C.read(2);
   MC = I2C.read(2);
   MD = I2C.read(2);
}

/* GET UNCOMPENSATED TEMPERATURE */
/*********************************/
void Barom::getUT()
{
   uint8_t I2Cframe[] = {Register.measure, Register.temperature};
   I2C.write(I2Cframe, 2);
   delayMicroseconds(4500); // Wait for temperature calculation
   
   I2C.write(Register.MSB);
   UT = I2C.read() << 8;
   I2C.write(Register.LSB);
   UT |= I2C.read();
}

/* GET UNCOMPENSATED PRESSURE */
/******************************/
void Barom::getUP()
{
   uint8_t I2Cframe[] = {Register.measure, Register.pressure | (OSS << 6)};
   I2C.write(I2Cframe, 2);
   
   // Wait for pressure calculation
   switch (OSS)
      {
         case Low:
         {
            delayMicroseconds(4500);
            break;
         }
         case Standard:
         {
            delayMicroseconds(7500);
            break;
         }
         case High:
         {
            delayMicroseconds(13500);
            break;
         }
         case Ultra:
         {
            delayMicroseconds(25500);
            break;
         }
      }
      
   I2C.write(Register.MSB);
   if (OSS == Ultra) delayMicroseconds(76500); // Extra compuational delay for (OSS == 3). See BMP180 datasheet section 3.3.2.
   UP = I2C.read() << 16;
   
   I2C.write(Register.LSB);
   UP |= I2C.read() << 8;
   
   if (OSS != Low)
   {
      I2C.write(Register.XLSB);
      UP |= I2C.read();
   }
   
   UP >>= 8 - OSS;
}

/* GET TEMPERATURE. DEFAULT ARGUMENT: FAHRENHEIT */
/*************************************************/
float Barom::getTemperature(Unit::Temperature tempUnit)
{
   getUT();
   float temperature;
   // GET COEFFICIENTS
   {
      // These magic numbers come directly from the Bosch datasheet and are not explained.
      /************************************/
      X1 = (UT - AC6) * AC5 / (1L << 15);
      X2 = (MC * (1L << 11)) / (X1 + MD);
      BB5 = X1 + X2;
      temperature = (BB5 + 8.0) / 160.0;
      /************************************/
   }

   convertTemperature(&temperature, Unit::C, tempUnit);
   return temperature;
}

/* GET PRESSURE. DEFAULT ARGUMENT: INCHES OF MERCURY*/
/****************************************************/
float Barom::getPressure(Unit::Pressure pressUnit, bool atSeaLevel)
{
   float tempInKelvin = getTemperature(Unit::K);
   getUP();
   // GET COEFFICIENTS
   {
      // These magic numbers come directly from the Bosch datasheet and are not explained.
      /************************************/
      BB6 = BB5 - 4000;
      X1 = (BB2 * (BB6 * BB6 / (1 << 12))) / (1 << 11);
      X2 = (AC2 * BB6) / (1 << 11);
      X3 = X1 + X2;
      BB3 = (((AC1 * 4 + X3) << OSS) + 2) / 4;
      X1 = AC3 * BB6 / (1 << 13);
      X2 = (BB1 * (BB6 * BB6 / (1 << 12))) / (1L << 16);
      X3 = ((X1 + X2) + 2) / (1 << 2);
      BB4 = AC4 * (uint32_t)(X3 + 32768) >> 15;
      BB7 = (UP - BB3) * (50000 >> OSS);
      pascals = (BB7 >> 31) ? (BB7 * 2) / BB4 : (BB7 / BB4) * 2;
      X1 = (pascals / (1 << 8)) * (pascals / (1 << 8));
      X1 =  (X1 * 3038) / (1L << 16);
      X2 = (-7357 * pascals) / (1L << 16);
      pascals += (X1 + X2 + 3791) / 16;
      /************************************/
   }
   
   // ADJUST FOR SEA LEVEL. https://en.wikipedia.org/wiki/Barometric_formula
   float pressure = (atSeaLevel) ? pascals / pow(1 - (lapseRate * elevInMeters / tempInKelvin), BaromExponent) : pascals;
   
   convertPressure(&pressure, Unit::Pa, pressUnit);
   return pressure;
}

/* GET ELEVATION.  IF CURRENT PRESSURE IS UNKNOWN, DEFAULT TO 1 ATM */
/********************************************************************/
float Barom::getElevation(float seaPress, Unit::Pressure pressUnit, Unit::Elevation elevUnit)
{
   convertPressure(&seaPress, pressUnit, Unit::Pa);
   float measuredPress = getPressure(Unit::Pa, false); // Non-adjusted pressure.
   
   // DERIVED FROM BAROMETRIC FORMULA. https://en.wikipedia.org/wiki/Barometric_formula
   float elevation = (getTemperature(Unit::K) * pow((seaPress / measuredPress), (-1 / BaromExponent)) * (pow((seaPress / measuredPress), (1 / BaromExponent)) - 1)) / lapseRate;
   
   convertElevation(&elevation, Unit::meters, elevUnit);
   return elevation;
}

/* CONVERT BETWEEN PRESSURE UNITS */
/**********************************/
void Barom::convertPressure(float *pressure, Unit::Pressure fromUnit, Unit::Pressure toUnit)
{
   const float mbConv =   100.0;
   const float mmHgConv = 133.322368;
   const float inHgConv = 3386.38816;
   const float atmConv =  101325.0;
   
   if (fromUnit != toUnit)
   {
      // Convert to Pascals
      switch (fromUnit)
      {
         case Unit::hPa:
         case Unit::mb:
         {
            *pressure *= mbConv;
            break;
         }
         case Unit::mmHg:
         {
            *pressure *= mmHgConv;
            break;
         }
         case Unit::inHg:
         {
            *pressure *= inHgConv;
            break;
         }
         case Unit::atm:
         {
            *pressure *= atmConv;
            break;
         }
      }
      
      // Convert from Pascals
      switch (toUnit)
      {
         case Unit::hPa:
         case Unit::mb:
         {
            *pressure /= mbConv;
            break;
         }
         case Unit::mmHg:
         {
            *pressure /= mmHgConv;
            break;
         }
         case Unit::inHg:
         {
            *pressure /= inHgConv;
            break;
         }
         case Unit::atm:
         {
            *pressure /= atmConv;
            break;
         }
      }
   }
}

/* CONVERT BETWEEN ELEVATION UNITS */
/***********************************/
void Barom::convertElevation(float *elevation, Unit::Elevation fromUnit, Unit::Elevation toUnit)
{
   const float ftMeterConv = 3.28083;
   if (fromUnit != toUnit)
   {
      switch (fromUnit)
      {
         case Unit::feet:
         {
            *elevation /= ftMeterConv;
            break;
         }
         case Unit::meters:
         {
            *elevation *= ftMeterConv;
            break;
         }
      }
   }
}

/* CONVERT BETWEEN TEMPERATURE UNITS */
/*************************************/
void Barom::convertTemperature(float *temperature, Unit::Temperature fromUnit, Unit::Temperature toUnit)
{
   const float kelvinScale = 273.15;
   const float fahrenMult = 1.8;
   const float fahrenScale = 32.0;
   
   if (fromUnit != toUnit)
   {
      // Convert to Celcius
      switch (fromUnit)
      {
         case Unit::K:
         {
            *temperature -= kelvinScale;
            break;
         }
         case Unit::F:
         {
            *temperature = (*temperature - fahrenScale) / fahrenMult;
            break;
         }
      }
      
      // Convert from Celcius
      switch (toUnit)
      {
         case Unit::K:
         {
            *temperature += kelvinScale;
            break;
         }
         case Unit::F:
         {
            *temperature = *temperature * fahrenMult + fahrenScale;
            break;
         }
      }
   }
}