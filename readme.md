# Barometric pressure sensor library for Arduino Uno #
## For use with [Bosch BMP-085/180/183](https://cdn-shop.adafruit.com/datasheets/BST-BMP180-DS000-09.pdf) sensors ##

* * *

### DEPENDENCIES ###

* Arduino Uno

* I2C Library

This sensor library requires the I2C library which is publicly available in my BitBucket archive.

* * *

### INITIALIZATION ###

1. Pressure units

    When retrieving pressure, it is important to pass in one of the following arguments to specify which units are desired:

    * mmHg
    * inHg (default)
    * hPa
    * Pa
    * mb
    * atm    

1. Elevation

    In order to retrieve the adjusted sea-level pressure, it is important to pass in both the elevation (altitude) and the units of elevation.  The following is a list of available units:

    * feet (default)
    * meters

1. Oversampling Setting (OSS)

    The OSS is a value which corresponds to the precision of the barometric pressure reading.  With higher values, more readings are taken internally, averaged, and returned to the user.  The trade-off is that higher readings take longer and consume more power.  The argument options are:
    
    * Low
    * Standard
    * High
    * Ultra (default)
    
One could create an instance of the Barom class like so:
``` {.cpp}
// Define I2C clock and data pins
const int clockPin = 1, int dataPin = 2;
// Without elevation, pressure cannot be adjusted to sea-level.
// If you are using this device as an altitude monitor, this value is irrelevant.
const float elevation = 1000;
Barom Barometer(clockPin, dataPin, elevation, Barom::Unit::feet, Barom::Ultra);
```
Or alternatively
``` {.cpp}
Barom Barometer(clockPin, dataPin, elevation);
```
Since "feet" and "Ultra" are default arguments.

* * *

### GETTING PRESSURE ###

Here are some examples for getting pressure:

``` {.cpp}
// Default unit - inHg.
float seaLevelPressure_inHg = Barometer.getPressure();

// Request pressure in mmHg.
float seaLevelPressure_mmHg = Barometer.getPressure(Barom::Unit::mmHg);

// Request absolute pressure; false => don't adjust for sea level.
float absolutePressure_inHg = Barometer.getPressure(Barom::Unit::inHg, false);
```

### GETTING TEMPERATURE ###

The barometer has a built-in temperature sensor which is used in converting absolute pressure to sea-level pressure.  Although better options exist for getting temperature, it can be handy to utilize the built-in temperature sensor.  Here are some examples:
``` {.cpp}
// Default unit: fahrenheit
float tempF = Barometer.getTemperature();

// Request temp in celcius
float tempC = Barometer.getTemperature(Barom::Unit::C);
```

### GETTING ELEVATION ###

Getting elevation (or altitude) is a little difficult because it requires the known sea-level-adjusted pressure for your area.  This information can be found for cities, but in rural area or in a moving environment this can be tricky.  If this value isn't known, the default is 1.0 atm which could throw your readings off by as much as 1,000 feet.  This could work if all you need is relative elevation and not absolute.

Examples:
``` {.cpp}
// Present known sea-level adjusted pressure.
// Can be any unit, found on most weather sites.
float currentAdjustedPressure_mb = 995.8;

// Get elevation in feet, submit pressure in millibars (mb)
float elevationInFeet = Barometer.getElevation(Barom::Unit::feet, currentAdjustedPressure_mb, Barom::Unit::mb);

// Get elevation in meters, submit pressure in millibars (mb)
float elevationInMeters = Barometer.getElevation(Barom::Unit::meters, currentAdjustedPressure_mb, Barom::Unit::mb);

// Get rough elevation, without knowing current pressure.
float roughElevInFeet = Barometer.getElevation(); // feet is default unit.
float roughElevInMeters = Barometer.getElevation(Barom::Unit::meters);
```