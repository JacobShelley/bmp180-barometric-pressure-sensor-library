#ifndef I2C_H
#define I2C_H
#include <arduino.h>

/*
Software I2C library.
Uses 7-bit addressing system to access slaves.
*/

class I2C
{
public:
   I2C(int8_t clockPin, int8_t dataPin, int8_t address7bit);
   I2C() {}; // Must use init() method to initialize pinout and address.

   void     init(int8_t clockPin, int8_t dataPin, int8_t address7bit); // Use after I2C() to initialize
   void     write(uint8_t writeFrame[], uint8_t writeByteQty);         // Submit multi-byte frame
   void     write(uint8_t writeReg);            // Submit single-byte frame
   uint32_t read(uint8_t QtyOfBytesToRead = 1); // Read X number of bytes
   
   
private:
   enum BusLine
   {
      Clock,
      Data,
   };
   
   struct SlaveDeviceAddress
   {
      uint8_t read;
      uint8_t write;
   } ModuleAddress;

   int8_t clockPin, dataPin;
   bool ackSlaveFail;
   
   // PRIVATE METHODS
   void driveLine(BusLine busLine, bool state); // Drive the bus (clock or data line) low or high
   uint8_t readByte();              // Read one byte
   void writeByte(uint8_t outByte); // Write one byte
   void writeBit(bool outBit);      // Write one bit
   bool readBit();                  // Read one bit
   void start(); // Start frame
   void stop();  // Stop frame
};

#endif