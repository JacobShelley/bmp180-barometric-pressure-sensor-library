// Bosch BMP180 barometric pressure sensor library for Arduino Uno.

#ifndef BAROM_H
#define BAROM_H
#include <arduino.h>
#include "I2C.h"

class Barom
{
public:
   struct Unit
   {
      enum Temperature
      {
         C,
         F,
         K
      };
      
      enum Pressure
      {
         mmHg,
         inHg,
         hPa,
         Pa,
         mb,
         atm
      };

      enum Elevation
      {
         feet,
         meters
      };
   };
   
   enum OSSmode
   {
      Low = 0,
      Standard = 1,
      High = 2,
      Ultra = 3
   } OSS;
   
   Barom(uint8_t SCLpin = 11, uint8_t SDApin = 12, float elevation = 915, Unit::Elevation elevUnit = Unit::feet, OSSmode OSS = Ultra);
   float getTemperature(Unit::Temperature tempUnit = Unit::F);
   float getPressure(Unit::Pressure pressUnit = Unit::inHg, bool atSeaLevel = true);
   float getElevation(float seaPress = 29.92126, Unit::Pressure pressUnit = Unit::inHg, Unit::Elevation elevUnit = Unit::feet); // 29.92 inHg = 1 ATM
   
private:
   float elevInMeters;
   // Barometric formula constants
   const float lapseRate = 0.0065, BaromExponent = 5.25578;
   // Calibration parameters.  Datasheet: pg. 15
   uint16_t AC4, AC5, AC6;
   int16_t  AC1, AC2, AC3, BB1, BB2, MB,  MC, MD;
   int32_t  X1,  X2,  X3,  BB3, BB5, BB6, pascals, UT;
   uint32_t BB4, BB7, UP;
   
   void convertElevation(float *elevation, Unit::Elevation fromUnit, Unit::Elevation toUnit);
   void convertPressure(float *pressure, Unit::Pressure fromUnit, Unit::Pressure toUnit);
   void convertTemperature(float *temperature, Unit::Temperature fromUnit, Unit::Temperature toUnit);
   void getUT();
   void getUP();
 
   struct Registers
   {
      const uint8_t barometer =   0x77;
      const uint8_t measure =     0xF4;
      const uint8_t temperature = 0x2E;
      const uint8_t pressure =    0x34;
      const uint8_t MSB =         0xF6;
      const uint8_t LSB =         0xF7;
      const uint8_t XLSB =        0xF8;
   } Register;
};

#endif